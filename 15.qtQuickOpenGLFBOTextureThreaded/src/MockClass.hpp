#pragma once

#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QQuickWindow>
#include <QOpenGLFunctions>

#include <QMatrix4x4>

// REMARK: This time we are going to render to a FBO
// We don't need anymore Q_OBJECT
// Or to have slots
class TriangleRenderer : protected QOpenGLFunctions
{

public:
    TriangleRenderer() : m_time(0), m_program(0) { }
    ~TriangleRenderer() {
		m_VAOTriangle->destroy(); 
		delete m_program;
	}

	void initialize() {
		/* We need to initialize the OpenGL functions here */
		initializeOpenGLFunctions();

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_COLOR_BUFFER_BIT);
		glEnable(GL_DEPTH_BUFFER_BIT);

		const char* vertexShaderSrc = "#version 330 core\n"
			"layout(location = 0) in vec3 position;\n"
			"uniform mat4 M;\n"
			"out vec4 v_pos;\n"
			"void main() {\n"
			"  v_pos = M*vec4(position.x, position.y, position.z, 1.0);\n"
			"  gl_Position = v_pos;\n"
			"}\n";
		const char* fragmentShaderSrc = "#version 330 core\n"
			"out vec4 color;\n"
			"in vec4 v_pos;\n"
			"void main()\n"
			"{\n"
			"  color = v_pos;\n"
			"}\n";

		// Making the vertex and fragment shader
		QOpenGLShader m_vertexShader(QOpenGLShader::Vertex);
		m_vertexShader.compileSourceCode(vertexShaderSrc);
		QOpenGLShader m_fragmentShader(QOpenGLShader::Fragment);
		m_fragmentShader.compileSourceCode(fragmentShaderSrc);

		// Making the shader program
		m_program = new QOpenGLShaderProgram(); // We can put the context as parameter here
												// We also could have used : 
												// m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSrc);
		m_program->addShader(&m_vertexShader);
		m_program->addShader(&m_fragmentShader);
		m_program->link();
		m_program->bind();

		GLfloat vertices[] = {
			-0.5f, -0.5f, 0.0f, // Left  
			0.5f, -0.5f, 0.0f, // Right 
			0.0f,  0.5f, 0.0f  // Top   
		};

		QOpenGLBuffer VBOTriangle(QOpenGLBuffer::VertexBuffer);
		VBOTriangle.create();
		VBOTriangle.bind(); // Do not release before creating the VAO!
		VBOTriangle.setUsagePattern(QOpenGLBuffer::StaticDraw);
		VBOTriangle.allocate(vertices, sizeof(vertices));

		m_VAOTriangle = new QOpenGLVertexArrayObject();
		m_VAOTriangle->create();
		m_VAOTriangle->bind();
		m_program->enableAttributeArray(0);
		m_program->setAttributeBuffer(0, GL_FLOAT, 0, 3, 0);
		m_VAOTriangle->release();

		// VBOTriangle.release();
		// VBOTriangle.destroy();

		// m_program->release();

		glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());


		m_time = 0;
	}

    void render() {

		glClearColor(0, 0, 0.25, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		m_program->bind();
		QMatrix4x4 model; // new identity model
		model.rotate(m_time % 360, QVector3D(0, 0, 1));
		
		int modelLocation = m_program->uniformLocation("M");
		m_program->setUniformValue(modelLocation, model);
		m_VAOTriangle->bind();
		glDrawArrays(GL_TRIANGLES, 0, 3);
		m_VAOTriangle->release();
		m_program->disableAttributeArray(0);
		m_program->release();

		m_time += 1;
    }

private:
    QSize m_viewportSize;
    int m_time;
    QOpenGLShaderProgram *m_program;
	QOpenGLVertexArrayObject *m_VAOTriangle;
};


#include <QQuickWindow>
#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLFramebufferObject>
#include <QQuickFramebufferObject>

class TriangleInFBORenderer : public QQuickFramebufferObject::Renderer {
public:
	TriangleInFBORenderer() {
		triangle.initialize();
	}

	void render() {
		triangle.render();
		update(); // TODO comment
	}

	QOpenGLFramebufferObject * createFramebufferObject(const QSize &size) {
		QOpenGLFramebufferObjectFormat format;
		format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
		format.setSamples(4);
		return new QOpenGLFramebufferObject(size, format);
	}

private:
	TriangleRenderer triangle;
};

#include <QObject>
#include <QQuickFramebufferObject>


class FBOMockClass : public QQuickFramebufferObject {
    Q_OBJECT
    // We don't need anymore the property as the QML don't change the m_time value. It is totally indipendent.
	//Q_PROPERTY(qreal t READ t WRITE setT NOTIFY tChanged)
public:
    FBOMockClass() {}

	Renderer *createRenderer() const {
		return new TriangleInFBORenderer();
	}
};
