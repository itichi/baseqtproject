#pragma once

#include <QObject>
#include <QWidget>

#include <QOpenGLShaderProgram>
#include <QOpenGLShader>

#include "glad/glad.h"

class GLWidget : public QWidget {
	Q_OBJECT
public:
	explicit GLWidget(QWidget * parent = 0);

	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void teardownGL();

private:
	//QMatrix4x4 m_projection;



    // now we have simply unsigned integers to represent our variables
    GLuint *m_VAOTriangle;

    // But I keep QOpenGLShaderProgram, this is not mandatory, but you'll have to compile your shaders in pure OpenGL
	QOpenGLShaderProgram *m_program;

};