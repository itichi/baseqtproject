#include "glwidget.h"

GLWidget::GLWidget(QWidget *parent)
	: QGLWidget(parent) {
	connect(&timer, SIGNAL(timeout()), this, SLOT(updateGL()));
	timer.start(16);
}

void GLWidget::initializeGL() {
	glClearColor(0.25, 0.25, 0.30, 1);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glRotatef(0.5, 1, 1, 1);
	glBegin(GL_TRIANGLES);
	glColor3f(1, 0, 0);	glVertex3f(-0.5, -0.5, 0.0);
	glColor3f(0, 1, 0);	glVertex3f(0.5, -0.5, 0.0);
	glColor3f(0, 0, 1);	glVertex3f(0.0, 0.5, 0.0);
	glEnd();
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
}
