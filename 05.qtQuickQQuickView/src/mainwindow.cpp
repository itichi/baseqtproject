#include "mainwindow.h"


MainWindow::MainWindow(int argc, char *argv[])
{
    app = new QGuiApplication(argc, argv);
	view = new QQuickView();

}

void MainWindow::viewSetSource(const QString filePath) {
	view->setSource(QUrl::fromLocalFile(filePath));
	QObject *item = view->rootObject();
	QObject::connect(item, SIGNAL(quit()), QGuiApplication::instance(), SLOT(quit()));
}

void MainWindow::show() {
	view->show();
}

int MainWindow::exec() {
	return app->exec();
}

MainWindow::~MainWindow()
{
	// /!\ Don't delete them explicitly! Qt will do it!
    //delete app;
	//delete engine;
}
