#include "mainwindow.h"

int main(int argc, char *argv[])
{
    MainWindow w(argc, argv);
	
	w.loadQMLFile("QQmlEngine.qml");

    return w.exec();
}