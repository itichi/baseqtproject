#pragma once

#include <QObject>

class MockClass : public QObject {
    Q_OBJECT
    Q_PROPERTY(int value READ getValue WRITE setValue NOTIFY valueChanged REVISION 1)
public:
    MockClass(QObject * parent = 0) : QObject(parent) {}

    int getValue() { return val; }
    void setValue(int v) { val = v; };

signals:
    Q_REVISION(1) void valueChanged();

private:
    int val = 4;
};