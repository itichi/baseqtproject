#include "mainwindow.h"

int main(int argc, char *argv[])
{
    MainWindow w(argc, argv);
	
	w.loadQMLFile(".", "qrc:/QQmlApplicationEngine.qml");

    return w.exec();
}