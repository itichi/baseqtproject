#include "glwidget.h"

#include <QOpenGLContext>
#include <QSurfaceFormat>

GLWidget::GLWidget(QWidget *parent)
	: QWidget(parent) {
	QSurfaceFormat format;
	format.setVersion(4, 3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);

    // We create our own OpenGL context, not threaded
	QOpenGLContext *context = new QOpenGLContext;
	//QOpenGLContext *shareContext = openglWidget->context();
	context->setFormat(format);
	//context->setShareContext(shareContext);
	context->create();
	//context->moveToThread(workerThread);
}



void GLWidget::initializeGL() {
    // We don't need anymore to initialize the OpenGL functions trought Qt as they are initialized with glad
	//initializeOpenGLFunctions();

	glEnable(GL_COLOR_BUFFER_BIT);
	glEnable(GL_DEPTH_BUFFER_BIT);

	const char* vertexShaderSrc = "#version 330 core\n"
		"layout(location = 0) in vec3 position;\n"
		"out vec4 v_pos;\n"
		"void main() {\n"
		"  v_pos = vec4(position.x, position.y, position.z, 1.0);\n"
		"  gl_Position = v_pos;\n"
		"}\n";
	const char* fragmentShaderSrc = "#version 330 core\n"
		"out vec4 color;\n"
		"in vec4 v_pos;\n"
		"void main()\n"
		"{\n"
		"  color = v_pos;\n"
		"}\n";

	// Making the vertex and fragment shader
	QOpenGLShader m_vertexShader(QOpenGLShader::Vertex);
	m_vertexShader.compileSourceCode(vertexShaderSrc);
	QOpenGLShader m_fragmentShader(QOpenGLShader::Fragment);
	m_fragmentShader.compileSourceCode(fragmentShaderSrc);

	// Making the shader program
	m_program = new QOpenGLShaderProgram(); // We can put the context as parameter here
	// We also could have used : 
	// m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSrc);
	m_program->addShader(&m_vertexShader);
	m_program->addShader(&m_fragmentShader);
	m_program->link();
	m_program->bind();

	GLfloat vertices[] = {
		-0.5f, -0.5f, 0.0f, // Left  
		0.5f, -0.5f, 0.0f, // Right 
		0.0f,  0.5f, 0.0f  // Top   
	};
	
	QOpenGLBuffer VBOTriangle(QOpenGLBuffer::VertexBuffer);
	VBOTriangle.create();
	VBOTriangle.bind(); // Do not release before creating the VAO!
	VBOTriangle.setUsagePattern(QOpenGLBuffer::StaticDraw);
	VBOTriangle.allocate(vertices, sizeof(vertices));

	m_VAOTriangle = new QOpenGLVertexArrayObject();
	m_VAOTriangle->create();
	m_VAOTriangle->bind();
	m_program->enableAttributeArray(0);
	m_program->setAttributeBuffer(0, GL_FLOAT, 0, 3, 0);
	m_VAOTriangle->release();

	// VBOTriangle.release();
	// VBOTriangle.destroy();

	// m_program->release();
}

void GLWidget::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
	//m_projection.setToIdentity();
	//m_projection.perspective(60.0f, w / float(h), 0.01f, 1000.0f);
}

void GLWidget::paintGL() {
	glClearColor(0, 0, 0.25, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_program->bind();
	m_VAOTriangle->bind();
	glDrawArrays(GL_TRIANGLES, 0, 3);
	m_VAOTriangle->release();
	m_program->release();


}

void GLWidget::teardownGL() {
	m_VAOTriangle->destroy();
	delete m_program;
}