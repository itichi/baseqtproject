#pragma once

class MockClass {
public:
    MockClass() {}

    int getValue() { return val; }
    void setValue(int v) { val = v; };

private:
    int val = 42;
};
