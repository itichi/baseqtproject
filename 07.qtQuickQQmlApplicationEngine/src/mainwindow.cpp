#include "mainwindow.h"

#include <QQmlComponent>

MainWindow::MainWindow(int argc, char *argv[])
{
    app = new QGuiApplication(argc, argv);
	engine = new QQmlApplicationEngine();
}

void MainWindow::loadQMLFile(const QString libraryPath, const QString filePath) {
	QCoreApplication::addLibraryPath(libraryPath);
	engine->load(QUrl(filePath));
}

void MainWindow::loadQMLComponent(const QString path) {
/*	QQmlComponent component(engine, QUrl(path));
	QObject *childItem = component.create();
	QList<QObject *> rootObjects = engine->rootObjects();
	QObject *parentItem = rootObjects.first();
	childItem->setParent(parentItem);
	QQmlEngine::setObjectOwnership(childItem, QQmlEngine::CppOwnership);
	*/
}

int MainWindow::exec() {
	return app->exec();
}

MainWindow::~MainWindow()
{
	// /!\ Don't delete them explicitly! Qt will do it!
    //delete app;
	//delete engine;
}
