#pragma once

#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQmlComponent>

namespace Ui {
class MainWindow;
}

class MainWindow {
public:
    explicit MainWindow(int argc, char *argv[]);
    ~MainWindow();
	
	void loadQMLFile(const QString filePath);
	int exec();

private:
    QGuiApplication *app;
	QQmlEngine *engine;
    QQmlContext *context;
    QQmlComponent *component;

    QObject *object;
};
