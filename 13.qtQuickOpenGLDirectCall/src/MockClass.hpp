#pragma once

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QQuickWindow>

#include <QMatrix4x4>

class MockClassRenderer : public QObject, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    MockClassRenderer() : m_time(0), m_program(0) { }
    ~MockClassRenderer() {
		m_VAOTriangle->destroy(); 
		delete m_program;
	}

    void setT(qreal t) { m_time = t; }
    void setViewportSize(const QSize &size) { m_viewportSize = size; }
    void setWindow(QQuickWindow *window) { m_window = window; }

    public slots:
    void paint() {
        if (!m_program) {
            /* We initialize the shader program here to be sure that the OpenGL context is bound and that we are on the correct thread */
            initializeOpenGLFunctions();

			glEnable(GL_COLOR_BUFFER_BIT);
			glEnable(GL_DEPTH_BUFFER_BIT);

			const char* vertexShaderSrc = "#version 330 core\n"
				"layout(location = 0) in vec3 position;\n"
				"uniform mat4 M;\n"
				"out vec4 v_pos;\n"
				"void main() {\n"
				"  v_pos = M*vec4(position.x, position.y, position.z, 1.0);\n"
				"  gl_Position = v_pos;\n"
				"}\n";
			const char* fragmentShaderSrc = "#version 330 core\n"
				"out vec4 color;\n"
				"in vec4 v_pos;\n"
				"void main()\n"
				"{\n"
				"  color = v_pos;\n"
				"}\n";

			// Making the vertex and fragment shader
			QOpenGLShader m_vertexShader(QOpenGLShader::Vertex);
			m_vertexShader.compileSourceCode(vertexShaderSrc);
			QOpenGLShader m_fragmentShader(QOpenGLShader::Fragment);
			m_fragmentShader.compileSourceCode(fragmentShaderSrc);

			// Making the shader program
			m_program = new QOpenGLShaderProgram(); // We can put the context as parameter here
													// We also could have used : 
													// m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSrc);
			m_program->addShader(&m_vertexShader);
			m_program->addShader(&m_fragmentShader);
			m_program->link();
			m_program->bind();

			GLfloat vertices[] = {
				-0.5f, -0.5f, 0.0f, // Left  
				0.5f, -0.5f, 0.0f, // Right 
				0.0f,  0.5f, 0.0f  // Top   
			};

			QOpenGLBuffer VBOTriangle(QOpenGLBuffer::VertexBuffer);
			VBOTriangle.create();
			VBOTriangle.bind(); // Do not release before creating the VAO!
			VBOTriangle.setUsagePattern(QOpenGLBuffer::StaticDraw);
			VBOTriangle.allocate(vertices, sizeof(vertices));

			m_VAOTriangle = new QOpenGLVertexArrayObject();
			m_VAOTriangle->create();
			m_VAOTriangle->bind();
			m_program->enableAttributeArray(0);
			m_program->setAttributeBuffer(0, GL_FLOAT, 0, 3, 0);
			m_VAOTriangle->release();

			// VBOTriangle.release();
			// VBOTriangle.destroy();

			// m_program->release();
        }


        glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());

        glEnable(GL_DEPTH_TEST);

		glClearColor(0, 0, 0.25, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_program->bind();
		QMatrix4x4 model; // new identity model
		model.rotate(m_time*1000, QVector3D(0, 0, 1));
		//qWarning() << m_time*1000 << " " << model;
		
		int modelLocation = m_program->uniformLocation("M");
		m_program->setUniformValue(modelLocation, model);
		m_VAOTriangle->bind();
		glDrawArrays(GL_TRIANGLES, 0, 3);
		m_VAOTriangle->release();
		m_program->disableAttributeArray(0);
		m_program->release();


		glDisable(GL_DEPTH_TEST);
		
		// Not strictly needed for this example, but generally useful for when
        // mixing with raw OpenGL.
        m_window->resetOpenGLState();
    }

private:
    QSize m_viewportSize;
    qreal m_time;
    QOpenGLShaderProgram *m_program;
	QOpenGLVertexArrayObject *m_VAOTriangle;
    QQuickWindow *m_window;
};

#include <QObject>
#include <QQuickItem>


class MockClass : public QQuickItem {
    Q_OBJECT
        Q_PROPERTY(qreal t READ t WRITE setT NOTIFY tChanged)
public:
    MockClass() : m_time(0), m_renderer(0) {
        connect(this, &QQuickItem::windowChanged, this, &MockClass::handleWindowChanged);
    }

    qreal t() const { return m_time; }
    void setT(qreal t) {
        if (t == m_time)
            return;
        m_time = t;
        emit tChanged();
        if (window())
            window()->update();
    }

signals:
    void tChanged();

    public slots:
    void sync() {
        if (!m_renderer) {
            m_renderer = new MockClassRenderer();
            connect(window(), &QQuickWindow::beforeRendering, m_renderer, &MockClassRenderer::paint, Qt::DirectConnection);
        }
        m_renderer->setViewportSize(window()->size() * window()->devicePixelRatio());
        m_renderer->setT(m_time);
        m_renderer->setWindow(window());
    }
    void cleanup() {
        if (m_renderer) {
            delete m_renderer;
            m_renderer = 0;
        }
    }

private slots:
    void handleWindowChanged(QQuickWindow *win) {
        if (win) {
            connect(win, &QQuickWindow::beforeSynchronizing, this, &MockClass::sync, Qt::DirectConnection);
            connect(win, &QQuickWindow::sceneGraphInvalidated, this, &MockClass::cleanup, Qt::DirectConnection);
            /*
            Note: Since the MockClass object has affinity to the GUI thread and the signals are emitted from the rendering thread, it is crucial that the connections are made with Qt::DirectConnection. Failing to do so, will result in that the slots are invoked on the wrong thread with no OpenGL context present.
            */
            win->setClearBeforeRendering(false);
            /*
            The default behavior of the scene graph is to clear the framebuffer before rendering. Since we render before the scene graph, we need to turn this clearing off. This means that we need to clear ourselves in the paint() function.
            */
        }
    }

private:
    qreal m_time;
    MockClassRenderer *m_renderer;
};
