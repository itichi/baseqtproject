#include "mainwindow.h"

#include <QQmlComponent>

MainWindow::MainWindow(int argc, char *argv[])
{
    app = new QGuiApplication(argc, argv);
	
    engine = new QQmlEngine();
    context = new QQmlContext(engine->rootContext());
}

void MainWindow::loadQMLFile(const QString filePath) {
	component = new QQmlComponent(engine, filePath);
    object = component->create(context);
}

int MainWindow::exec() {
	int ret = app->exec();

    delete object;
    delete context;

    return ret;
}

MainWindow::~MainWindow()
{
}
