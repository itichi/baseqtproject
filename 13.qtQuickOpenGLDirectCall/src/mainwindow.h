#pragma once

#include <QGuiApplication>
#include <QQmlApplicationEngine>

namespace Ui {
class MainWindow;
}

class MainWindow {
public:
    explicit MainWindow(int argc, char *argv[]);
    ~MainWindow();
	
    void setContext();
	void loadQMLFile(const QString libraryPath, const QString filePath);
	void loadQMLComponent(const QString path);
	int exec();

private:
    QGuiApplication *app;
	QQmlApplicationEngine *engine;
	//QQmlContext
};
