#include "mainwindow.h"

int main(int argc, char *argv[])
{
    MainWindow w(argc, argv);
	
	w.viewSetSource("QQuickView.qml");
	w.show();

    return w.exec();
}