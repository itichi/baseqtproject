#include "mainwindow.h"
#include "main.h"

int main(int argc, char *argv[])
{
    MainWindow w(argc, argv);

    w.setContext();

	w.loadQMLFile(".", "qrc:/qtQuickOpenGLDirectCall.qml");

    return w.exec();
}