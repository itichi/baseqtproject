#pragma once

#include <QGLWidget>
#include <QTimer>

class GLWidget : public QGLWidget {
	Q_OBJECT
public:
	explicit GLWidget(QWidget * parent = 0);

	void initializeGL();
	void paintGL();
	void resizeGL(int w, int h);
private:
	void gluPerspective(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar);


private:
	QTimer timer;
};