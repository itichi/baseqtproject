#pragma once

#include <QGuiApplication>
#include <QQuickView>
#include <QQuickItem>

namespace Ui {
class MainWindow;
}

class MainWindow {
public:
    explicit MainWindow(int argc, char *argv[]);
    ~MainWindow();
	
	void viewSetSource(const QString filePath);
	void show();
	int exec();

private:
    QGuiApplication *app;
	QQuickView *view;
};
