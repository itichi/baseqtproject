# BaseQtProject

This repository is intended to show how to make *base* Qt projects working with Cmake. Those projects are easely tweakables according to a project needs.

I personally use visual studio 2015 with the plugin **Qt Visual Studio Tools** from *The Qt Company Ltd.*

When the plugin is properly configured, by clicking on a *.ui* file into VS, it should open Qt Designer.



tested on Qt version : 5.8, plugin 2.1.1, VS 2015 (2017.03)



## Differences between the projects

### Ui Designer

The first 4 projects are intended to work with .ui files.

#### 01: Simply load a Qt ui file, and display it.

ui files can be created into Qt Designer

#### 02: Do OpenGL rendering by using QGLWidget (old), it is fixed functions OpenGL.

This project shouldn't be used anymore after 2016

#### 03: QOpenGLWidget, this class is the replacement of QGLWidget for modern OpenGL/Qt applications

#### 04: This project is intended to use OpenGL functions directly, without using the Qt Wrappers.

This is done (will be done) by using glad.c

### Qt Quick

The Next projects are intended to work with QML files, QML documents are loaded and executed by the QML runtime. This include the Declarative UI engine along with the built-in QML types and plugins modules, and it also provides access to third-party QML types and modules.

Applications that use QML need to invoke the QML runtime in order to execute QML documents. This can be done by creating a [QQuickView](http://doc.qt.io/qt-5/qquickview.html) or a [QQmlEngine](http://doc.qt.io/qt-5/qqmlengine.html). In addition, the Declarative UI package includes the qmlscene tool, which loads `.qml` files. This tool is useful for developing and testing QML code without the need to write a C++ application to load the QML runtime.

When you compile with VS, the qml is actually transformed into a C++ code and compiled with the executable, this is the reason you don't need to include the qml file with your software.

[Deploying QML Applications](http://doc.qt.io/qt-5/qtquick-deployment.html)



#### 05: Using a QQuickView. QQuickView is a QWindow-based class that is able to load QML files.

**With QQuickView, you can use an Item as Top Level in QML, this is not the case for the project 07**

[What's the difference between QQuickView and QQuickWindow?](http://stackoverflow.com/questions/23936169/whats-the-difference-between-qquickview-and-qquickwindow)

> The QQuickView class provides a window for displaying a Qt Quick user interface.
>
> QQuickView is a convenience subclass of QQuickWindow which will automatically load and display a QML scene when given the URL of the main source file.

So QQuickView is a subclass of QQuickWindow which manages displaying a scene from a QML file and could be used easily like:

```
QQuickView *view = new QQuickView;
view->setSource(QUrl::fromLocalFile("myqmlfile.qml"));
view->show();
```

For displaying a graphical QML scene in a window you can also use the QQuickWindow class.

Also from the Qt documentation:

> A QQuickWindow always has a single invisible root item. To add items to this window, reparent the items to the root item or to an existing item in the scene.

So it can be used like:

```
QQmlApplicationEngine engine;
engine.load(QUrl("myqmlfile.qml"));

QObject *topLevel = engine.rootObjects().value(0);
QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);

window->show();
```
#### 06: We can use the QQmlEngine directly **if our QML does not have any graphical components**, or if we want to avoid QQuickView. The ```.qml``` file is loaded as a QQmlComponent instance rather than placed into a view. 

*QGuiApplication* can be replaced by *QCoreApplication* in the code in case you are not using any graphical items from Qt Quick (use QML as a language without any dependencies to the Qt Gui module)

#### 07: Using QQmlApplicationEngine

[What is the difference between QQmlApplicationEngine and QQuickView?](http://stackoverflow.com/questions/40153361/what-is-the-difference-between-qqmlapplicationengine-and-qquickview) 

Headline: QQmlApplicationEngine is newer and more powerful than QQuickView.

QQmlApplicationEngine exposes some central application functionality to QML, which QQuickView application would normally control from C++:

> - Connecting Qt.quit() to QCoreApplication::quit()
> - Automatically loads translation files from an i18n directory adjacent to the main QML file.
> - Automatically sets an incubation controller if the scene contains a QQuickWindow.
> - Automatically sets a QQmlFileSelector as the url interceptor, applying file selectors to all QML files and assets.

Ref: [Qt docs](http://doc.qt.io/qt-5/qqmlapplicationengine.html)

At the time when QQmlApplicationEngine was introduced, the [Qt Blog had this to say](http://blog.qt.io/blog/2013/06/21/overview-of-the-new-features-in-qt-quick/):

> In Qt 5.0 we generally created Qt Quick applications by declaring a QQuickView in C++ and setting the base url on it. The drawback of that approach is that you have to use C++ to set properties like width, height etc. In Qt 5.1 we encourage using Window or ApplicationWindow as the root item of your application, giving complete control to Qt Quick, so we are now introducing the QQmlApplicationEngine to make this use case a little bit simpler. The QmlApplicationEngine is all you need to set up your qt quick window, pick up the right translation files and it implicitly connects the application quit() signal to your root window.

Qt Quick Controls 2.0 is able to make use of this extra application control, through the new item [ApplicationWindow](https://doc-snapshots.qt.io/qt5-5.7/qml-qtquick-controls2-applicationwindow.html), which:

- is similar to the regular QQuickWindow, but adds support for setting a window specific MenuBar, ToolBar and StatusBar in QML.
- makes it convenient to add a header and footer item to the window.
- makes it possible to control the window's properties, appearance and layout from QML.
- supports popups via its overlay property, which ensures that popups are displayed above other content and that the background is dimmed when a modal popup is visible.

So, in order to use some of the Qt Quick Controls features like MenuBar and Popup, we need to:

- use ApplicationWindow as our top-level QML item instead of Rectangle or Item
- use the new QQmlApplicationEngine to load the QML from C++ instead of the old QQuickView.

**REMARK: With the QQmlApplication engine YOU MUST USE A Window AS TOP LEVEL ITEM**

You can use both together if you don't want your top level item to be a `Window`.

```
QQmlApplcationEngine engine;
QQuickView view(&engine, 0);
// your usual engine code
view.show();
```
#### Exposing Attributes of C++ Types to QML

https://qmlbook.github.io/en/ch16/index.html

There are 3 ways of exposing attributes of C++ types to QML:

- Context properties - ```setContextProperty()```
- Register type with engine - calling ```qmlRegisterType``` in your main.cpp
- QML extension plugins (Best way but more tedious)



#### Interacting with QML Objects from C++

<u>(DON'T DO THAT - cf: Effective QML by Thomas McGuire KDAB)</u>

#### 08: Exposing C++ class attributes to QML without using the macro Q_OBJECT

The idea here is to:

- instantiate the class in C++
- get the engine context (engine->rootContext()) and
- add a property using
  setContextProperty("nameOfThePropertyInTheQMLFile", MyClass->getTheAttribute);

We continue to use the QQmlApplicationEngine

#### 09: Exposing a C++ class to QML by using the Q_OBJECT macro

The Q_OBJECT macro allows reflexion in Qt.

We want here to use an entire object trought QML. We first declare our object as a QObject, after that, we are going to expose the properties we want in QML by using

```
Q_PROPERTY(return_type name_in_QML READ fun2ReadTheValue WRITE fun2SetTheVal NOTIFY signal2SayThatSomethingHappened)
```

for example:

```
Q_PROPERTY(int value READ value WRITE setValue NOTIFY valChanged)
```

In our window, we only need to instantiate the object and set it as a property

#### 10: Exposing a C++ class to QML by registering it as a QML type

Here, we can directly use our class as a QML type inside QML code:

```
import com.domain 1.0 # contain the MyClassAlias
MyClassAlias {
	id: mc
}
Rectangle {
	Text {
		anchors.centerIn: parent
		text: "Hello World " + mc.value			
	}
}
```

We still need to define the Q_PROPERTYes.

This time, we add our object by using 

```
qmlRegisterType<MyClass, 1>("com.domain", 1, 0, "MyClassAlias");
```



### Android

There is a project to work with android, If you don't have the SDK & NDK, desactivate the android option in Cmake.

The idea here is to work with OpenGL ES 2.0 and the corresponding version for the desktop

### OpenGL

#### Mixing Scene Graph and OpenGL

The scene graph offers two methods for integrating OpenGL content: by calling OpenGL commands directly and by creating a textured node in the scene graph.

By connecting to the [QQuickWindow::beforeRendering](http://doc.qt.io/qt-5/qquickwindow.html#beforeRendering)() and [QQuickWindow::afterRendering](http://doc.qt.io/qt-5/qquickwindow.html#afterRendering)() signals, applications can make OpenGL calls directly into the same context as the scene graph is rendering to. As the signal names indicate, the user can then render OpenGL content either under a Qt Quick scene or over it. The benefit of integrating in this manner is that no extra framebuffer nor memory is needed to perform the rendering. The downside is that Qt Quick decides when to call the signals and this is the only time the OpenGL application is allowed to draw.

The [Scene Graph - OpenGL Under QML](http://doc.qt.io/qt-5/qtquick-scenegraph-openglunderqml-example.html) example gives an example on how to use these signals.

The other alternative is to create a [QQuickFramebufferObject](http://doc.qt.io/qt-5/qquickframebufferobject.html), render into it, and let it be displayed in the scene graph as a texture. The [Scene Graph - Rendering FBOs](http://doc.qt.io/qt-5/qtquick-scenegraph-textureinsgnode-example.html) example shows how this can be done. It is also possible to combine multiple rendering contexts and multiple threads to create content to be displayed in the scene graph. The [Scene Graph - Rendering FBOs in a thread](http://doc.qt.io/qt-5/qtquick-scenegraph-textureinthread-example.html) examples show how this can be done.

**Warning:** When mixing OpenGL content with scene graph rendering, it is important the application does not leave the OpenGL context in a state with buffers bound, attributes enabled, special values in the z-buffer or stencil-buffer or similar. Doing so can result in unpredictable behavior.

**Warning:** The OpenGL rendering code must be thread aware, as the rendering might be happening outside the GUI thread.

#### Custom Items using QPainter

The [QQuickItem](http://doc.qt.io/qt-5/qquickitem.html) provides a subclass, [QQuickPaintedItem](http://doc.qt.io/qt-5/qquickpainteditem.html), which allows the users to render content using [QPainter](http://doc.qt.io/qt-5/qpainter.html).

**Warning:** Using [QQuickPaintedItem](http://doc.qt.io/qt-5/qquickpainteditem.html) uses an indirect 2D surface to render its content, either using software rasterization or using an OpenGL framebuffer object (FBO), so the rendering is a two-step operation. First rasterize the surface, then draw the surface. Using scene graph API directly is always significantly faster.

http://doc.qt.io/qt-5/qtquick-visualcanvas-scenegraph.html

#### 13: QML and OpenGL with direct OpenGL calls in the same context as the scene graph (QML)

We use [QQuickWindow::beforeRendering](http://doc.qt.io/qt-5/qquickwindow.html#beforeRendering)() and/or [QQuickWindow::afterRendering](http://doc.qt.io/qt-5/qquickwindow.html#afterRendering)() signals to call directly OpenGL in a QML scene, the drawback is that QT Quick decides when to call the signals, OpenGL is not allowed to draw at any time.



We use a QQuickItem as base element for our visual OpenGL item. The [QQuickItem](http://doc.qt.io/qt-5/qquickitem.html) class provides the most basic of all visual items in [Qt Quick](http://doc.qt.io/qt-5/qtquick-index.html). All visual items in Qt Quick inherit from [QQuickItem](http://doc.qt.io/qt-5/qquickitem.html). Although a [QQuickItem](http://doc.qt.io/qt-5/qquickitem.html) instance has no visual appearance, it defines all the attributes that are common across visual items, such as x and y position, width and height, [anchoring](http://doc.qt.io/qt-5/qtquick-positioning-anchors.html) and key handling support.

http://doc.qt.io/qt-5/qquickitem.html#details



#### 14: QML and OpenGL with Render to Texture before showing the texture in QML element

Here we make a Framebuffer Object (in Qt), render to it and display it as a texture in the scene graph.

The advantage is that the rendering is totally independent from a Qt Quick call.

### Comparison

| Class | OpenGL | Android |      |      |
| ----- | ------ | ------- | ---- | ---- |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |
|       |        |         |      |      |



## install

- Download the project
- create a new git project
- push the base files you want to use
- start your project