import QtQuick 2.5
import QtQuick.Window 2.2
import com.itichi 1.0

// main window
Window {	
    visible: true
    MouseArea {
        anchors.fill: parent
        onClicked: {
            	Qt.quit();
        }
    }

    Mock {
		id: mockclass
	}
	
	Rectangle {
		anchors.fill: parent
		Text {
			anchors.centerIn: parent
			text: "Hello World " + mockclass.value			
		}
	}

}


