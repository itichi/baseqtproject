#include "mainwindow.h"

int main(int argc, char *argv[])
{
    MainWindow w(argc, argv);

    w.setContext();

    w.loadQMLFile(".", "qrc:/QQmlExposingCppAttributesContextPropertiesNoQOBJECT.qml");

    return w.exec();
}