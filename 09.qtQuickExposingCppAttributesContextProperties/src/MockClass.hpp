#pragma once

#include <QObject>

class MockClass : public QObject {

	Q_OBJECT
	Q_PROPERTY(int value READ value WRITE setValue NOTIFY valChanged)

public:
    MockClass(QObject * parent = 0) : QObject(parent) {}

    int value() { return m_val; }
    void setValue(int v) {
		if (m_val != v) {
			m_val = v;
			emit valChanged();
		}
	};

signals:
	void valChanged();

private:
    int m_val = 4;
};