#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

#include <QOpenGLShaderProgram>

#include <QOpenGLVertexArrayObject>


#include <QMatrix4x4>

/* QOpenGLWidget vs QOpenGLWindow (from http://stackoverflow.com/questions/17860604/what-is-the-difference-between-a-qwindow-and-qwidget)
What is the difference between a QWindow and QWidget
---

The Qt 5.0 provides a new QWindow class. While the documentation on this class is quite comprehensive,
I am failing to see how exactly the QWindow is different from the QWidget class,
and in which cases you would prefer the former. Both provide a handy way of visualising all sorts of
things to the screen, both can use QPainter for drawing, and both have a way to interact with OpenGL.

In the API description, it says that:

An application will typically use QWidget or QQuickView for its UI, and not QWindow directly.
So that doesn't seem to be an advantage for the window. Moreover, it states:

Windows can potentially use a lot of memory. A usual measurement is width times height times color depth.
A window might also include multiple buffers to support double and triple buffering, as well as depth and stencil buffers.
Which doesn't seem to be in favour of using the QWindow. So in what cases would you use it?

---

QWindow has been introduced in Qt 5.0 due to the gui / widgets split. QWidget now lives in its own library (QtWidgets);
it was necessary to provide the abstraction of a "toplevel window" for non-widgets based applications,
and thus QWindow was created -- and lives in QtGui.

There is an entire class of non-widgets based applications: all those using QtQuick2.
They don't use the QtWidget library at all, and as a matter of fact, when using them you're
always somehow using QWindows explicitely (QQuickView inherits from QWindow).

Even when using widgets, top-level QWindows get created for you by the Qt kernel,
which also keeps the properties and the flags of such QWindow objects in sync with the corresponding top-level QWidgets.
This way you can just deal with widgets like you always did, without knowing about QWindow at all.
Existing applications will continue to run as expected, etc. etc.

The only reason (so far) I've been using QWindows explicitely is for a very specific use case:
to draw pure OpenGL content. This is very easy to achieve (by setting an OpenGL surface type on the window),
and avoids you to bring in additional dependencies (QtWidgets, QtOpenGL, etc., which have a cost in terms of library size);
it allows to create a OpenGL drawing surface in like 10 lines of code which will work on Linux, Windows, Mac, QNX, "embedded Linux",
and very likely Android and iOS too. From this point of view it acts as a perfect SDL replacement. :)

*/


class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions {
	Q_OBJECT
public:
	explicit GLWidget(QWidget * parent = 0);

	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void teardownGL();

private:
	//QMatrix4x4 m_projection;

	QOpenGLShaderProgram *m_program;

	QOpenGLVertexArrayObject *m_VAOTriangle;

};