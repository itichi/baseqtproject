import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.1

Window {
    width: 360
    height: 272
    visible: true
    MouseArea {
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.fill: parent
        onClicked: {
                Qt.quit();
        }
    }

    Rectangle {
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.fill: parent

      Slider {
          id: slider
          x: 80
          y: 35
          stepSize: 1
          to: 50
          from: -20
          value: mockClass.value

          onValueChanged: {
              mockClass.value = slider.value
          }
      }

      SpinBox {
          id: spinBox
          x: 110
          y: 100
           from: -20
          to: 50
          value: mockClass.value

          onValueChanged: {
              mockClass.value = spinBox.value
          }
      }

      Button {
          id: button
          x: 130
          y: 182
          text: qsTr("Reset")
          spacing: -4

          onClicked: {
              mockClass.value = 42
          }
      }

    }
}


