import QtQuick 2.4
import QtQuick.Window 2.2

Rectangle {
    anchors.fill: parent
	signal quit()
    Text {
        anchors.centerIn: parent
        text: "Hello World"
    }
    MouseArea {
        enabled: true
        anchors.fill: parent
        onClicked: {
	        quit()
        }
    }
}


